FROM golang:1.9-alpine as builder
WORKDIR /go/src/github.com/samisagit/snitch/main
ENV SRC_DIR /go/src/github.com/samisagit/snitch/
COPY . $SRC_DIR
RUN CGO_ENABLED=0 GOOS=linux go build ./

FROM alpine
RUN apk add --no-cache ca-certificates apache2-utils
COPY --from=builder /go/src/github.com/samisagit/snitch/main/main .
ENTRYPOINT ["./main"]