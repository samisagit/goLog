package auth

import (
	b64 "encoding/base64"
	"encoding/json"
	"errors"
	"github.com/samisagit/snitch/config"
	"strings"
)

func ReceiveAuthReqest(body []byte, secret string) (string, error) {
	var ar authRequest
	err := json.Unmarshal(body, &ar)
	if err != nil {
		return "", err
	}
	pass, err := GetPassword(ar.AccountId)
	if err != nil {
		return "", err
	}
	if pass != HashString(ar.Password, config.PasswordHash) {
		return "", errors.New("password invalid")
	}

	return ar.generateJwt().toString(secret), nil
}

func GetAccountIdFromPayload(s string) (int, error) {
	arr := strings.Split(s, ".")
	payload, err := b64.StdEncoding.DecodeString(arr[1])
	if err != nil {
		return 0, err
	}
	var data struct {
		AccountId   int    `json:accountId`
		Environment string `json:environment`
	}
	json.Unmarshal(payload, &data)

	return data.AccountId, nil
}
