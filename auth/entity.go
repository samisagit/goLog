package auth

import (
	"crypto/hmac"
	"crypto/sha256"
	b64 "encoding/base64"
	"encoding/hex"
	"fmt"
	"strings"
)

type IPersistentStore interface {
	GetPassword(int) (string, error)
	GetName(int) (string, error)
}

type authRequest struct {
	AccountId int    `json:AccountId`
	Password  string `json:password`
}

func (a authRequest) generateJwt() jwt {
	j := new(jwt)
	j.Header.Alg = "HS256"
	j.Header.Typ = "JWT"
	j.Payload.Id = a.AccountId
	return *j
}

type jwt struct {
	Header struct {
		Alg string `json:alg`
		Typ string `json:typ`
	}
	Payload struct {
		Id          int    `json:accountId`
		Environment string `json:environment`
	}
}

func (j jwt) toString(secret string) string {
	header := fmt.Sprintf(
		"{\"alg\": \"%s\",",
		j.Header.Alg) +
		fmt.Sprintf(
			"\"typ\": \"%s\"}",
			j.Header.Typ)

	payload := fmt.Sprintf(
		"{\"accountId\": %d,",
		j.Payload.Id) +
		fmt.Sprintf(
			"\"environment\": \"%s\"}",
			j.Payload.Environment)

	key := []byte(secret)
	sig := hmac.New(sha256.New, key)
	sig.Write([]byte(payload))
	signature := hex.EncodeToString(sig.Sum(nil))

	header = b64.StdEncoding.EncodeToString([]byte(header))

	payload = b64.StdEncoding.EncodeToString([]byte(payload))

	return fmt.Sprintf("%s.%s.%s", header, payload, signature)
}

func AuthenticateJwt(j string, secret string) bool {
	arr := strings.Split(j, ".")
	payload, err := b64.StdEncoding.DecodeString(arr[1])
	if err != nil {
		return false
	}
	key := []byte(secret)
	sig := hmac.New(sha256.New, key)
	sig.Write(payload)
	signature := hex.EncodeToString(sig.Sum(nil))
	if signature != arr[2] {
		return false
	}
	return true
}

func HashString(p string, secret string) string {
	key := []byte(secret)
	sig := hmac.New(sha256.New, key)
	sig.Write([]byte(p))
	return hex.EncodeToString(sig.Sum(nil))
}
