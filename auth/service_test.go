package auth

import (
	"github.com/samisagit/snitch/config"
	"testing"
)

func TestGetAccountIdFromPayload(t *testing.T) {
	ar := authRequest{
		AccountId: 123,
		Password:  "1fsxagwegl%",
	}
	mockArTocken := ar.generateJwt().toString(config.JwtSecret)
	id, err := GetAccountIdFromPayload(mockArTocken)
	if err != nil {
		t.Error("threw err from GetAccountIdFromPayload with valid token")
	}
	if id != 123 {
		t.Errorf("pulled incorrect AccountId (%d) from token", id)
	}
}

func TestGetAccountIdFromPayloadInvalidToken(t *testing.T) {
	mockArTocken := "lknfa¬~@?jsndkjasd.dsdas¬~@?das.sda¬~@?sdsadasda"
	_, err := GetAccountIdFromPayload(mockArTocken)
	if err == nil {
		t.Error("invalid token should throw an error")
	}
}
