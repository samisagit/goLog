package server

import (
	"github.com/gorilla/mux"
	"github.com/samisagit/snitch/server/routes"
	"net/http"
)

func ConfigureRoutes(r *mux.Router) {
	r.Methods("OPTIONS").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")
		w.WriteHeader(http.StatusNoContent)
		return
	})

	routes.LogRoutes(r.PathPrefix("/logs").Subrouter())
	routes.UserRoutes(r.PathPrefix("/users").Subrouter())
}
