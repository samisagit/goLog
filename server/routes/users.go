package routes

import (
	"github.com/gorilla/mux"
	"github.com/samisagit/snitch/auth"
	"github.com/samisagit/snitch/config"
	"github.com/samisagit/snitch/middleware"
	"io/ioutil"
	"net/http"
)

func UserRoutes(r *mux.Router) {
	r.Use(middleware.RoutePanic, middleware.Cors)

	r.PathPrefix("/auth").Methods("POST").HandlerFunc(handleLogin).Name("auth")
}

func handleLogin(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
		w.WriteHeader(http.StatusBadGateway)
		return
	}

	jwt, err := auth.ReceiveAuthReqest(body, config.JwtSecret)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jwt))
}
