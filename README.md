### ensure go is installed correctly and $GOPATH is set
https://golang.org/doc/install

https://github.com/golang/go/wiki/SettingGOPATH

### clone repository into valid $GOPATH
`cd /$GOPATH/src/github.com/src` (mkdir if not created yet)

`git clone git@gitlab.com:samisagit/snitch.git`

### install dependencies
`cd /$GOPATH`

`go get -u github.com/kardianos/govendor`

`govendor init`

`govendor add +external`

### ensure docker/docker-compose are installed correctly
https://docs.docker.com/install/

https://docs.docker.com/compose/install/

### serve project on localhost:81
`cd /$GOPATH/src/github.com/snitch`

`docker-compose up --build`