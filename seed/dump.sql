create database if not exists logger;
grant all privileges on logger.* to 'logger_api'@'%' identified by 'password';
use logger;

create table if not exists users (
	id int auto_increment
		primary key,
	name varchar(255) not null unique,
	password mediumtext not null
);

create table if not exists logs (
	id int auto_increment
		primary key,
	app tinytext not null,
	message longtext not null,
	severity tinytext not null,
	environment tinytext not null,
	time bigint not null,
	stackTrace longtext null
);

insert ignore into users (
  name,
  password
) values (
  'test app',
  '87368da3adb3d1ac6dde8479361f008f43b84342c35b7fac4d7d86ac6bc1a4d0'
);

insert into logs (
  app,
  message,
  severity,
  environment,
  time,
  stackTrace
) values (
  'test app',
  'test error',
  'error',
  'staging',
  current_timestamp,
  'test stack trace'
);