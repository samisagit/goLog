package middleware

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/samisagit/snitch/auth"
	"github.com/samisagit/snitch/config"
	"net/http"
)

var Cors = mux.MiddlewareFunc(setUpCors)
var Jwt = mux.MiddlewareFunc(interceptJwt)
var RoutePanic = mux.MiddlewareFunc(handlePanicInRoute)

// If this is called (so far every request ATOW) we need to hit the
// whitelist cache to see if the origin is allowed, otherwise bin them.
func setUpCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("origin"))
		w.Header().Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
		w.Header().Add("Access-Control-Allow-Methods", "OPTIONS, POST, GET, PUT, DELETE")
		if r.Method == "OPTIONS" {
			w.WriteHeader(200)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// If this is called we need to authorize the user, so we'll take the
// authorization header and hand it off to the auth package.
func interceptJwt(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		j := r.Header.Get("Authorization")
		if j == "" {
			w.WriteHeader(404)
			w.Write([]byte("No JWT"))
			return
		}
		if !auth.AuthenticateJwt(j, config.JwtSecret) {
			w.WriteHeader(404)
			w.Write([]byte("Invalid JWT"))
			return
		}
		next.ServeHTTP(w, r)
	})
}

func handlePanicInRoute(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer serverErrorRecovery(w)
		next.ServeHTTP(w, r)
	})
}

func serverErrorRecovery(w http.ResponseWriter) {
	if r := recover(); r != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprintf("Server error occured: %v", r)))
	}
}
