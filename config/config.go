package config

import (
	"os"
)

// Set up MySQL DB credentials
var (
	MySqlHost     = os.Getenv("SQLHOST")
	MySqlUser     = os.Getenv("SQLUSER")
	MySqlPassword = os.Getenv("SQLPASS")
)

// JWT secret
var JwtSecret = os.Getenv("JWTSECRET")

// Slack API token
var SlackToken = os.Getenv("SLACKTOKEN")

// Password hash
var PasswordHash = os.Getenv("PASSWORDHASH")
