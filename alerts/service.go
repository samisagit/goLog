package alerts

type dispatchee interface {
	Id() int
	AppId() string
	GetEnvironment() string
	GetMessage() string
	GetTime() int32
}

func Dispatch(d dispatchee) error {
	messageItem := ConvertLog(d)
	err := messageItem.Send()
	return err
}
