package logger

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/samisagit/snitch/alerts"
)

func ReceiveLog(requestBody []byte, userId string) {
	logItem := Log{}
	logItem.SetAppId(userId)

	err := json.Unmarshal(requestBody, &logItem)
	if err != nil {
		panic(err)
	}

	errs := logItem.parse()
	if len(errs) > 0 {
		str := ""
		for _, e := range errs {
			str = str + fmt.Sprintf("%s\n", e.Error())
		}
		panic(str)
	}
	dispatchLog(logItem, HighPriorityQueue, LowPriorityQueue)
}

func (l Log) parse() []error {
	errorSlice := []error{}
	if l.Environment == "" {
		errorSlice = append(errorSlice, errors.New("environment is a required string field"))
	}
	if l.Severity == "" {
		errorSlice = append(errorSlice, errors.New("severity is a required string field"))
	}
	if l.Stack == "" {
		errorSlice = append(errorSlice, errors.New("stacktrace is a required string field"))
	}
	if l.Message == "" {
		errorSlice = append(errorSlice, errors.New("message is a required string field"))
	}
	if l.Time == 0 {
		errorSlice = append(errorSlice, errors.New("time is a required int field"))
	}
	return errorSlice
}

func dispatchLog(l Log, hpq chan Log, lpq chan Log) {
	if l.Environment == "production" && l.Severity == "fatal" {
		hpq <- l
		err := alerts.Dispatch(l)
		if err != nil {
			panic(err)
		}
	} else {
		lpq <- l
	}
}

func RetrieveLog(id int) Log {
	return FindOne(id)
}

func RetrievePaginatedLogs(filters string) []Log {
	return FindPaginated(filters)
}
