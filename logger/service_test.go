package logger

import (
	"fmt"
	"testing"
)

func TestReceiveLog(t *testing.T) {
	testCases := []struct {
		bs     []byte
		userId string
	}{
		{[]byte(`{"severity": "fatal", "message": "Error(Could not parse input).message", "stack": "Error(Could not parse input).stack", "time": 1000}`), "124"},
		{[]byte(``), "124"},
		{[]byte(`{}`), "124"},
		{[]byte(`{"fakeField": "fake val"}`), "124"},
	}

	for i, tc := range testCases {
		t.Run("missing field", func(t *testing.T) {
			defer func() {
				if r := recover(); r == nil {
				}
			}()
			ReceiveLog(tc.bs, tc.userId)
			t.Error(fmt.Sprintf("invalid Log should have caused panic in test %d", i))
		})
	}
}

func TestParseLog(t *testing.T) {

	paramSlice := []Log{
		{
			Environment: "",
			Severity:    "fatal",
			Stack:       "test stack",
			Message:     "test message",
			Time:        1234,
		},
		{
			Environment: "production",
			Severity:    "fatal",
			Stack:       "test stack",
			Message:     "",
			Time:        1234,
		},
		{
			Environment: "production",
			Severity:    "fatal",
			Stack:       "",
			Message:     "test message",
			Time:        1234,
		},
		{
			Environment: "production",
			Severity:    "fatal",
			Stack:       "test stack",
			Message:     "test message",
			Time:        0,
		},
		{
			Severity: "fatal",
			Stack:    "test stack",
			Message:  "test message",
			Time:     1234,
		},
		{},
	}

	t.Run("parsing logs with missing fields", func(t *testing.T) {
		for i, l := range paramSlice {
			errSlice := l.parse()
			if len(errSlice) == 0 {
				t.Errorf("parse called on log with missing field should cause err (case %d)", i)
			}
		}
	})
}

func TestDispatchLog(t *testing.T) {
	h := make(chan Log, 3)
	l := make(chan Log, 3)
	liveLog := Log{
		Environment: "production",
		Severity:    "low",
		Stack:       "test",
		Message:     "",
		Time:        0,
	}
	fatalLog := Log{
		Environment: "local",
		Severity:    "fatal",
		Stack:       "test",
		Message:     "",
		Time:        0,
	}
	liveFatalLog := Log{
		Environment: "production",
		Severity:    "fatal",
		Stack:       "test",
		Message:     "",
		Time:        0,
	}
	dispatchLog(liveLog, h, l)
	dispatchLog(fatalLog, h, l)
	dispatchLog(liveFatalLog, h, l)

	if len(h) != 1 {
		t.Error("production fatal log did not get prioritised")
	}

	if len(l) != 2 {
		t.Error("non urgent logs did not get queued")
	}
}
